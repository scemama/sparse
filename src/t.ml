include Lacaml.D
let zero = 0.
let one  = 1.

let is_null ~cutoff =
   assert (cutoff >= zero);
   if cutoff = zero then
      fun x -> x = zero
   else
      fun x -> (abs_float x) < cutoff

