(** Matrix stored in coordinate format.

Coordinate format is a list of triplets [(i,j,x)] where [i] is the row
index, [j] is the column index and [x] is the value.
It is easy to append new values to this format, because the triplets
are not necessarily ordered.
If multiple values have the same coordinates, the reduction operation
will make one entry summing all the values with the same coordinate,
transforming the type into [reduced t].
*)

type 'a t 
type triplet = (int * int * float)
 
(** Labels telling if the list is reduced *)
type unsorted
type reduced

(** Number of rows *)
val dim1 : 'a t -> int

(** Number of columns *)
val dim2 : 'a t -> int

(** Create from dimensions and an optional list of triplets *)
val create : m:int -> ?n:int -> ?data:triplet list -> ?reduced:bool -> unit -> reduced t

(** Create from a list of tuples *)
val of_triplet_list : ?reduced:bool -> triplet list -> reduced t

(** Conversion into a list of triplets *)
val to_triplet_list : 'a t -> triplet list

(** Create from a dense matrix. Values below |[cutoff]| will be discarded. *)
val of_mat : ?cutoff:float -> T.Mat.t -> reduced t

(** Conversion into a dense matrix *)
val to_mat : 'a t -> T.Mat.t

(** Swap row and column indices *)
val transpose : 'a t -> unsorted t

(** Reduce an unsorted matrix. Values below |[cutoff]| will be discarded. *)
val reduce : ?cutoff:float -> unsorted t -> reduced t

(** [at m i j] returns the value of [m] at coordinates [(i,j)] *)
val at : reduced t -> int -> int -> float 

(** [at_column m j] returns a function to get the i-th values of the j-th column
    of [m] *)
val at_column : reduced t -> int -> (int -> float)

(** Add two matrices *)
val add : ?alpha:float -> ?beta:float -> 'a t -> 'a t -> unsorted t

(** Scale matrix by a constant *)
val scale : 'a t -> float -> 'a t

(** Pretty printing *)
val pp : Format.formatter -> 'a t -> unit


