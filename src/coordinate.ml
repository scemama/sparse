type unsorted
type reduced

type triplet = int * int * float
             
type 'a t = {
    data : triplet list ;  (* List of triplets *)
    m  : int;              (* Number of rows    *)
    n  : int;              (* Number of columns *)
  }


let triplet_compare (i,j,x) (k,l,y) =
  if j < l then -1 else
  if j > l then  1 else
  if i < k then -1 else
  if i > k then  1 else
  if abs_float x < abs_float y then -1 else
  if abs_float x > abs_float y then  1 else
  0

let sort = List.fast_sort triplet_compare 


    

(** Access *)

let dim1 t    = t.m
let dim2 t    = t.n
let to_triplet_list t = t.data

let at_column t = 
  fun col ->
  assert (col >= 1);
  assert (col <= dim2 t);
  (* Search column *)
  let rec loop = function
    | (_,j,_) :: rest when j < col -> loop rest
    | (i,j,x) :: rest when j = col -> (i,j,x) :: rest
    | _ -> []
  in
  let column = loop t.data in
  
  fun row ->
  assert (row >= 1);
  assert (row <= dim1 t);
  let rec loop = function
    | (i,j,_) :: rest when i < row && j = col -> loop rest
    | (i,j,x) :: _    when i = row && j = col -> x
    | _ -> 0.
  in
  loop column
  
let at t i j = at_column t j i  
           

(** Finds dimensions of a list of triplets *)
let find_dimensions data =
  let rec loop mmax nmax = function
    | [] -> mmax, nmax
    | (i,j,_)::rest ->
       let m = max i mmax in
       let n = max j nmax in
       loop m n rest
  in
  loop 0 0 data



(** Creation *)

let reduce ?(cutoff=T.zero) t =
  let is_null = T.is_null ~cutoff in
  let rec loop accu = function
    | [] -> List.rev accu
    | triplet :: []   -> loop (triplet :: accu) []
    | t :: t' :: rest ->
       let (i,j,x) = t  in
       let (k,l,y) = t' in
       if i = k && j = l then
         let new_x = x +. y in
         if is_null new_x then
           loop accu rest
         else
           loop accu ( (i,j, new_x)::rest )
       else
         loop (t :: accu) (t' :: rest)
  in
  { t with
    data = sort t.data
           |> loop []
  }

let create ~m ?(n=1) ?(data=[]) ?(reduced=false) () =
  assert (m >= 0);
  assert (n >= 0);
  List.iter (fun (i,j,_) ->
      assert (i >= 1);
      assert (j >= 1);
      assert (i <= m);
      assert (j <= n);
    ) data;
  if reduced then 
    { data ; m ; n }
  else
    reduce { data ; m ; n }


let of_triplet_list ?(reduced=false) data =
  let m, n = find_dimensions data in
  create ~m ~n ~data ~reduced ()


let of_mat ?(cutoff=T.zero) mat =

  let m = T.Mat.dim1 mat in
  let n = T.Mat.dim2 mat in

  let is_null = T.is_null ~cutoff in

  let rec triplet_list_of_col accu mat j = function
    | 0 -> accu
    | i -> let new_accu =
             let x = mat.{i,j} in
             if is_null x then
               accu
             else
               (i,j,x)::accu
           in
           triplet_list_of_col new_accu mat j (i-1)
  in

  let data =
    let rec loop accu = function
      | 0 -> accu
      | j -> let new_accu =
               triplet_list_of_col accu mat j m
             in
             loop new_accu (j-1)
    in
    loop [] n
  in

  { data ; m ; n }


(** Conversion *)

let to_mat t =
  let result =
    T.Mat.init_cols t.m t.n (fun _ _ -> T.zero)
  in
  List.iter (fun (i,j,x) ->
      result.{i,j} <- result.{i,j} +. x
    ) t.data;
  result


let transpose t =
  { n = t.m ;
    m = t.n ;
    data = List.rev_map (fun (i,j,x) -> (j,i,x)) t.data
           |> List.rev ;
  }




(** Operations *)

let add ?(alpha=1.0) ?(beta=1.0) t t' =
  assert (dim1 t = dim1 t');
  assert (dim2 t = dim2 t');
  if alpha=1. && beta=1. then
    { t with
      data = List.rev_append t.data t'.data
    }
  else
    let l1 = List.rev_map (fun (i,j,x) -> (i,j,alpha *. x)) t.data  in
    let l2 = List.rev_map (fun (i,j,x) -> (i,j,beta  *. x)) t'.data in
    { t with
      data = List.rev_append l1 l2
    }
    


let scale t a =
  { t with
    data = List.rev_map (fun (i,j,x) -> (i,j,x*.a) ) t.data
           |> List.rev
  }



(** Pretty printers *)

let pp ppf t =
  Format.fprintf ppf "[@; @[m=%d@]@,@ @[n=%d@]@;" t.m t.n;
  List.iter (fun (i,j,x) ->
      Format.fprintf ppf "@[(%d,%d,%e),@]@," i j x
    ) t.data;
  Format.fprintf ppf "]@;"



