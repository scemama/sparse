(* Algorithms adapted from T.A. Davis, Direct Methods for Sparse Linear Systems. *)

type t = {
    x : float array;
    i : int array;
    p : int array;
    n : int ;
    m : int ;
  }


let dim1 t = t.m
let dim2 t = t.n
let non_zero t = Array.length t.x


let create ~m ~n () =
  { x = Array.make 0 0. ;
    i = Array.make 0 0  ;
    p = Array.make (n+1) 0 ;
    m ; n 
  }


let of_coordinate c =
  let m = Coordinate.dim1 c in
  let n = Coordinate.dim2 c in
  let data = Coordinate.to_triplet_list c in

  let nz = List.length data    in
  let p  = Array.make (n+1) 0  in
  let i  = Array.make nz    0  in
  let x  = Array.make nz    0. in

  let rec loop prev_col p_i k = function
    | (row, col, value) :: rest  when col = prev_col ->
       i.(k) <- row;
       x.(k) <- value;
       loop col p_i (k+1) rest
    | (row, col, value) :: rest  ->
       i.(k) <- row;
       x.(k) <- value;
       p.(p_i) <- k;
       loop col (p_i+1) (k+1) rest
    | [] -> p.(p_i) <- nz;
  in
  loop 0 0 0 data;

  { m ; n ; p ; i ; x }


let of_triplet_list ?(reduced=false) l =
  Coordinate.of_triplet_list ~reduced l
  |> of_coordinate


let of_mat ?(cutoff=T.zero) m =
  Coordinate.of_mat ~cutoff m
  |> of_coordinate



let get_column t j =
  assert (j >= 1);
  assert (j <= t.n);
  let start = t.p.(j-1) in
  let len   = t.p.(j) - t.p.(j-1) in
  { p = [| 0 ; len |] ;
    i = Array.sub t.i start len ;
    x = Array.sub t.x start len ;
    m = len ;
    n = 1 ;
  }


let to_coordinate t =
  let data =
    Array.init t.n (fun i -> i+1)
    |> Array.map (get_column t)
    |> Array.mapi (fun j' col ->
           let j = j' + 1 in
           Array.map2 (fun i x -> (i,j,x)) col.i col.x
           |> Array.to_list
         )
    |> Array.to_list
    |> List.concat
  in
  Coordinate.create ~m:t.m ~n:t.n ~data ()


let to_mat t =
  let mat = T.Mat.make0 (dim1 t) (dim2 t) in
  for j=1 to t.n do
    for k = t.p.(j-1) to t.p.(j)-1 do
      mat.{t.i.(k),j} <- t.x.(k)
    done
  done;
  mat


(* Page 14 *)
let transpose t =

  let nz = non_zero t in
  let m = t.m in
  let n = t.n in
  let p = Array.make (m+1) 0 in
  let i = Array.make nz 0 in
  let x = Array.make nz 0. in

  let w = Array.make m 0 in
  Array.iter (fun pi -> w.(pi-1) <- w.(pi-1)+1) t.i;
  let rec cumsum z i =
    if i>0 then
      w.(i-1) <- p.(i-1) ;
    p.(i) <- z;
    if i<m then
      cumsum (z+w.(i)) (i+1)
  in
  (*
  let cumsum z i = 
   let z = ref z in
   for i=i to (m-1) do
     p.(i) <- !z;
     z := !z + w.(i);
     w.(i) <- p.(i);
   done;
  in
  *)
  cumsum 0 0;
  for j=1 to n do
    for pp=t.p.(j-1) to t.p.(j)-1 do
      let ipp = t.i.(pp)-1 in
      let q = w.(ipp) in
      w.(ipp) <- w.(ipp) + 1;
      i.(q) <- j;
      x.(q) <- t.x.(pp);
    done
  done;
  { x ; i ; p ; m=n ; n=m }


(* Page 15 *)
let reduce ?(cutoff=T.zero) t =
  if cutoff = T.zero then t else
    let is_null = T.is_null ~cutoff in
    let nz =
      Array.fold_left (fun nz x -> if is_null x then nz else (nz+1)) 0 t.x
    in
    
    let m = t.m in
    let n = t.n in
    let p = Array.make (n+1) 0 in
    let i = Array.make nz 0 in
    let x = Array.make nz 0. in

    let nz = ref 0 in
    for j=1 to n do
      p.(j-1) <- !nz;
      for pp=t.p.(j-1) to t.p.(j)-1 do
        if not (is_null t.x.(pp)) then
          begin
            x.(!nz) <- t.x.(pp);
            i.(!nz) <- t.i.(pp);
            incr nz
          end
      done
    done;
    p.(n) <- !nz;
    { x ; i ; p ; m ; n }


exception ExitLoop of float

let at_column t =
  fun col ->
  let column = get_column t col in

  fun row ->
  assert (row >= 1);
  assert (row <= dim1 t);
  try
    for k=0 to non_zero column - 1 do
      if column.i.(k) > row then
        raise (ExitLoop 0.)
      else if column.i.(k) = row then
        raise (ExitLoop column.x.(k))
    done;
    0.
  with ExitLoop x -> x


let at t i j = at_column t j i


let scale t a =
  { t with
    x = Array.map (fun x -> x *. a) t.x
  }


(* Page 10 *)
let gemv ?(in_place=false) ?(alpha=1.0) ?(beta=0.0) ?y t x =
  assert (T.Vec.dim x = t.n);

  let y =
    match y with
    | None   -> T.Vec.init t.m (fun _ -> 0.)
    | Some v -> assert (T.Vec.dim v = t.m);
                if in_place then
                  begin
                    if beta <> 1. then
                      T.scal beta v;
                    v
                  end
                else
                  if beta = 1. then 
                    T.Vec.init t.m (fun i -> v.{i})
                  else
                    T.Vec.init t.m (fun i -> beta *. v.{i})
  in

  let p = t.p in
  let i = t.i in
  if alpha = 1.0 then

    for j=1 to t.n do
      for pp=p.(j-1) to p.(j)-1 do
        y.{i.(pp)} <- y.{i.(pp)} +. t.x.(pp) *. x.{j}
      done
    done

  else

    for j=1 to t.n do
      for pp=p.(j-1) to p.(j)-1 do
        y.{i.(pp)} <- y.{i.(pp)} +. alpha *. t.x.(pp) *. x.{j}
      done
    done

  ;
  y
  

(* Page 18 *)
let gemm ?(transa=`N) ?(transb=`N) a b =
              
  (* Here, we do AB=(Bt At)t to guarantee that the columns are sorted. *)
  let a, b = 
    match transa, transb with
    | `N, `N -> assert (a.n = b.m) ; (transpose b, transpose a)
    | `T, `T -> assert (a.m = b.n) ; (b, a)
    | `T, `N -> assert (a.m = b.m) ; (b, transpose a)
    | `N, `T -> assert (a.n = b.n) ; (transpose b, a)
  in
  
  let m = a.m in
  let n = b.n in

  let anz = Array.length a.x in
  let bnz = Array.length b.x in
  
  let p = Array.make (n+1) 0 in
  let i = Array.make (anz+bnz) 0 in
  let x = Array.make (anz+bnz) 0. in

  let extend_arrays k i x = 
    if k+m < Array.length x then
      (i,x)
    else
      ( Array.init (2*k+m) (fun l -> if l <= k then i.(l) else 0), 
        Array.init (2*k+m) (fun l -> if l <= k then x.(l) else 0.) )
  in

  let w = Array.make m 0 in
  let v = Array.make m 0. in
  
  let nz = ref 0 in
  for j=1 to n do
    
    let i,x = extend_arrays !nz i x in
    p.(j-1) <- !nz;

    for bp = b.p.(j-1) to b.p.(j)-1 do
      let k    = b.i.(bp) in
      let beta = b.x.(bp) in

      for ap = a.p.(k-1) to a.p.(k)-1 do
        let ii = a.i.(ap)-1 in
        if w.(ii) < j then
          begin
            w.(ii) <- j;
            i.(!nz) <- ii+1;
            incr nz;
            v.(ii) <- beta *. a.x.(ap);
          end
        else
          v.(ii) <- v.(ii) +. beta *. a.x.(ap)
      done;
      for cp = p.(j-1) to !nz-1 do
        x.(cp) <- v.(i.(cp)-1)
      done
      
    done

  done;
  p.(n) <- !nz;
  let i = Array.sub i 0 !nz in
  let x = Array.sub x 0 !nz in
  transpose { x ; i ; p ; m ; n }


(* Page 19 *)
let add ?(alpha=1.0) ?(beta=1.0) a b =  
  assert (a.m = b.m);
  assert (a.n = b.n);

  (* Here, we do A+B=(At+Bt)t to guarantee that the columns are sorted. *)
  let a, b = 
    transpose a, transpose b
  in
  
  let m = a.m in
  let n = a.n in

  let anz = Array.length a.x in
  let bnz = Array.length b.x in
  
  let p = Array.make (n+1) 0 in
  let i = Array.make (anz+bnz) 0 in
  let x = Array.make (anz+bnz) 0. in

  let w = Array.make m 0 in
  let v = Array.make m 0. in
  
  let nz = ref 0 in
  for j=1 to n do
    
    p.(j-1) <- !nz;

    for ap = a.p.(j-1) to a.p.(j)-1 do
      let ii = a.i.(ap)-1 in
      if w.(ii) < j then
        begin
          w.(ii) <- j;
          i.(!nz) <- ii+1;
          incr nz;
          v.(ii) <- alpha *. a.x.(ap);
        end
      else
        v.(ii) <- v.(ii) +. alpha *. a.x.(ap)
    done;

    for bp = b.p.(j-1) to b.p.(j)-1 do
      let ii = b.i.(bp)-1 in
      if w.(ii) < j then
        begin
          w.(ii) <- j;
          i.(!nz) <- ii+1;
          incr nz;
          v.(ii) <- beta *. b.x.(bp);
        end
      else
        v.(ii) <- v.(ii) +. beta *. b.x.(bp)
    done;
    
    for cp = p.(j-1) to !nz-1 do
      x.(cp) <- v.(i.(cp)-1)
    done
      
  done;
  p.(n) <- !nz;
  let i = Array.sub i 0 !nz in
  let x = Array.sub x 0 !nz in
  transpose { x ; i ; p ; m ; n }




(** Pretty printing *)
let pp ppf t =
  Format.fprintf ppf "[@; @[m=%d@]@,@ @[n=%d@]@;" t.m t.n;
  for j=1 to t.n do
    for k=t.p.(j-1) to t.p.(j)-1 do
      Format.fprintf ppf "@[(%d,%d,%e),@]@," t.i.(k) j t.x.(k);
    done
  done;
  Format.fprintf ppf "]@;"

