(** Matrix stored in compressed-column format.

Both column and row indices are sorted in ascending order,
and entries are unique.
*)

type t 

(** Number of rows *)
val dim1 : t -> int

(** Number of columns *)
val dim2 : t -> int

(** Create an empty matrix *)
val create : m:int -> n:int -> unit -> t

(** Create from a list of tuples *)
val of_triplet_list : ?reduced:bool -> Coordinate.triplet list -> t

(** Convert from coordinate format *)
val of_coordinate : Coordinate.reduced Coordinate.t -> t

(** Create from a dense matrix *)
val of_mat : ?cutoff:float -> T.mat -> t

(** Number of non-zero elements *)
val non_zero : t -> int

(** Conversion into a list of triplets *)
val to_coordinate : t -> Coordinate.reduced Coordinate.t

(** Conversion into a dense matrix *)
val to_mat : t -> T.Mat.t

(** [at m i j] returns the value of [m] at coordinates [(i,j)] *)
val at : t -> int -> int -> float

(** [at_column m j] returns a function to get the i-th values of the j-th column
    of [m] *)
val at_column : t -> int -> (int -> float)

(** Swap row and column indices *)
val transpose : t -> t

(** Reduce an unsorted matrix. Values below |[cutoff]| will be discarded. *)
val reduce : ?cutoff:float -> t -> t

(** Scale matrix by a constant *)
val scale : t -> float -> t

(* General matrix-vector product: [y <- alpha * A.x + beta*y]. If
   [in_place] is [true], the [y] vector is changed in place. *)
val gemv : ?in_place:bool -> ?alpha:float -> ?beta:float -> ?y:T.Vec.t ->
           t -> T.Vec.t -> T.Vec.t

(* General matrix multiplication. *)
val gemm : ?transa:[ `N | `T ] -> ?transb:[ `N | `T ] -> t -> t -> t

(* Matrix addition. [alpha*A + beta*B], Defaults for [alpha] and [beta] are 1. *)
val add : ?alpha:float -> ?beta:float -> t -> t -> t

(** Pretty printing *)
val pp : Format.formatter -> t -> unit

