open Sparse

let check msg x = Alcotest.(check bool) msg true x
  
(* List of tuples of non-zero entries *)
let triplet_list = 
  [ (1,2,0.07); (1,3,-.0.73); (2,1,-.0.08) ;
    (2,2,0.26) ; (3,2,-.0.10) ; (4,1,0.77) ]

let triplet_list' = 
  [ (1,1,-.0.17); (1,3,0.1); (2,3,0.18) ;
    (2,1,0.06) ; (3,2,-.0.10) ; (4,2,0.77) ]


(* Dense matrix created from the list of non-zero entries *)
let mat_of_dense t =
  let mat = 
    T.Mat.init_cols 4 3 (fun _ _ -> T.zero)
  in
  List.iter (fun (i,j,x) -> mat.{i,j} <- mat.{i,j} +. x) t;
  mat

let mat_dense  = mat_of_dense triplet_list
let mat_dense' = mat_of_dense triplet_list'


(* Sparse matrix created from the list of non-zero entries *)
let mat_sparse  = Coordinate.of_triplet_list triplet_list
let mat_sparse' = Coordinate.of_triplet_list triplet_list'
         


(* Tests *)
let test_structure () =
  check "Dimension 1" (Coordinate.dim1 mat_sparse = T.Mat.dim1 mat_dense);
  check "Dimension 2" (Coordinate.dim2 mat_sparse = T.Mat.dim2 mat_dense);
  check "Content" (Coordinate.of_mat mat_dense = mat_sparse);
  check "Access non-zero" (Coordinate.at mat_sparse 3 2 = -0.1);
  check "Access zero" (Coordinate.at mat_sparse 3 2 = mat_dense.{3,2});
  check "Access by column" (
      let get_row = Coordinate.at_column mat_sparse 3 in
      get_row 1 = mat_dense.{1,3} &&
      get_row 2 = mat_dense.{2,3} &&
      get_row 3 = mat_dense.{3,3} &&
      get_row 3 = mat_dense.{4,3} );
  ()

let test_modifications () =
  let mat_dense_t  = T.Mat.transpose_copy mat_dense  in
  let mat_sparse_t = Coordinate.transpose mat_sparse in
  check "Transpose" (Coordinate.reduce @@ mat_sparse_t = 
                     Coordinate.of_mat @@ mat_dense_t);

  check "Add" (Coordinate.reduce (Coordinate.add mat_sparse mat_sparse') =
               Coordinate.of_mat (T.Mat.add mat_dense mat_dense'));

  check "Scale" (Coordinate.scale mat_sparse 2.0 =
                 Coordinate.reduce (Coordinate.add mat_sparse mat_sparse));
  ()


let tests = [
  "Structure", `Quick, test_structure;
  "Modifications", `Quick, test_modifications;
]
