(*
   Run all the OCaml test suites defined in the project.
*)

let test_suites: unit Alcotest.test list = [
  "Coordinate", Test_coordinate.tests;
  "Csc", Test_csc.tests;
]

let () = Alcotest.run "sparse" test_suites
